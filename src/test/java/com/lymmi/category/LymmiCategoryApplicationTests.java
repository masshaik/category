package com.lymmi.category;

import com.lymmi.category.entity.Category;
import com.lymmi.category.entity.Product;
import com.lymmi.category.repository.CategoryRepository;
import com.lymmi.category.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class LymmiCategoryApplicationTests {

    @Test
    void contextLoads() {
    }


    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ProductRepository productRepository;


   // @Test
    public void createCategory() {


        Category category = new Category();
        category.setDescrption("description");
        category.setName("categoryName");

        Product product = new Product();
        product.setImageUrl("www.image.com");
        product.setPrice(300);
        product.setProductName("product name");

        // category.setProduct(product);

        categoryRepository.save(category);
    }


    //@Test
    public void createProduct() {


        Category category = new Category();
        category.setDescrption("description");
        category.setName("categoryName");

        Product product = new Product();
        product.setImageUrl("www.image.com");
        product.setPrice(300);
        product.setProductName("product name");

        // product.setCategory(category);

        productRepository.save(product);
    }

   // @Test
    public void getProduct() {
        Optional<Product> byId = productRepository.findById(6);
        System.out.println(byId.get());
    }


   // @Test
    public void createCategoryWithMultipleProducts() {


        Category category = new Category();
        category.setName("realme7pro");
        category.setDescrption("SuperfastCharge");

        Product product = new Product();
        product.setProductName("Realme7proBlack");
        product.setPrice(20000);
        product.setImageUrl("www.realmeblack.com");
        Product product1 = new Product();
        product1.setProductName("Realme7proBlack");
        product1.setPrice(20000);
        product1.setImageUrl("www.realmeblack.com");


        List<Product> products = new ArrayList<>();
        products.add(product);
        products.add(product1);

        category.setProduct(products);

        categoryRepository.save(category);
    }


    //@Test
    public void getCategoryProducts() {
        Optional<Category> by = categoryRepository.findById(4);

        List<Product> products = by.get().getProduct();
        products.stream().forEach(product -> System.out.println(product));
    }


    @Test
    public void createCategories() {
        Category category = new Category();
        category.setDescrption("good mobile");
        category.setName("name");


        Product product = new Product();
        product.setPrice(3000);
        product.setProductName("productName");
        product.setImageUrl("imageUrl");

        Product product1 = new Product();
        product1.setPrice(3000);
        product1.setProductName("product");
        product1.setImageUrl("www.com");

        List<Product> products = new ArrayList<>();
        products.add(product);
        products.add(product1);

        category.setProduct(products);

        categoryRepository.save(category);

    }


    @Test
    public void createProducts() {
        Category category = new Category();
        category.setDescrption("good mobile");
        category.setName("name");


        Category category1 = new Category();
        category.setDescrption("good mobile");
        category.setName("name");

        Product product = new Product();
        product.setPrice(3000);
        product.setProductName("productName");
        product.setImageUrl("imageUrl");

        Product product1 = new Product();
        product1.setPrice(3000);
        product1.setProductName("product");
        product1.setImageUrl("www.com");

        List<Product> products = new ArrayList<>();
        products.add(product);
        products.add(product1);

        category.setProduct(products);

        categoryRepository.save(category);

    }


}
