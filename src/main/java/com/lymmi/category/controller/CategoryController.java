package com.lymmi.category.controller;

import com.lymmi.category.CategoryService;
import com.lymmi.category.entity.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/category")
public class CategoryController {


    @Autowired
    CategoryService categoryService;


    @GetMapping("/get_category/{categoryId}")
    public ResponseEntity<Category> createCategory(@PathVariable("categoryId") int categoryId) {
        List<Category> categories = categoryService.getCategories();
        Optional<Category> categoryOptional = categories.stream().filter((category) -> category.getId() == categoryId).findAny();
        return new ResponseEntity(categoryOptional, HttpStatus.OK);
    }
}
