package com.lymmi.category.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String descrption;

    @ManyToMany(cascade = CascadeType.ALL,mappedBy = "categories")
    List<Product> product = new ArrayList<>();

    public Category(int id, String name, String descrption) {
        this.id = id;
        this.name = name;
        this.descrption = descrption;
    }
}
