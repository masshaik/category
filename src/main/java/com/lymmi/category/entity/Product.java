package com.lymmi.category.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

import com.lymmi.category.entity.Category;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table
@Data
public class Product {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int productId;
    private String productName;
    private double price;
    private String imageUrl;


    @ManyToMany(cascade = CascadeType.ALL)
    List<Category> categories;

}
