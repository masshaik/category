package com.lymmi.category;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LymmiCategoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(LymmiCategoryApplication.class, args);
	}

}
