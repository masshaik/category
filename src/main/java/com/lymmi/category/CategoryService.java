package com.lymmi.category;

import com.lymmi.category.entity.Category;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    public List<Category> getCategories() {
        List<Category> categories = new ArrayList<>();
        categories.add(new Category(1, "laptop", "Good Laptop"));
        categories.add(new Category(2, "mobile", "Good MObile"));
        categories.add(new Category(3, "keyboards", "Good Keyboards"));
        categories.add(new Category(4, "Racks", "Good Racks"));

        return categories;
    }
}
